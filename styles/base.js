export default {
  colors: {
    primary: "#2E6DB4", //-- Blue
    secondary: "#FFCB06", //-- Yellow
    highlight: "#FF0045" //-- Red
  },
  pastelColors: {
    blue: "#6DC3E8",
    purple: "#C390D4",
    green: "#AFDBAF",
    brown: "#DBC5AF"
  }
}