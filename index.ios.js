/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import {
  AppRegistry,
  StyleSheet,
  Navigator
} from 'react-native';
import React, { Component } from 'react';
import Splash from './components/splash/splash'
import WebDetails from './components/webDetails/webDetails'
import Map from './components/map/map'

class App extends Component {
  render() {
    return (
       <Navigator 
         initialRoute={{name: "Splash"}} 
         configureScene={configureTransitions}
         renderScene={renderNavigation} />
      );
  }
}

const configureTransitions = (route, routeStack) => { 
  return {
    ...route.transition || Navigator.SceneConfigs.HorizontalSwipeJump,
    gestures: route.gestures
  };
}

const renderNavigation = (route, navigator) => {
  if(route.name == 'Splash') {
     return <Splash navigator={navigator} {...route.passProps} />
  }
  if(route.name == 'Map' ) {
     return <Map navigator={navigator} {...route.passProps} />
  }
  if(route.name == 'Details' ) {
     return <WebDetails navigator={navigator} {...route.passProps} />
  } 
}

AppRegistry.registerComponent('Project', () => App);
