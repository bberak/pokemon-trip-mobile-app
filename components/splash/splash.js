import BaseStyles from '../../styles/base'
import NavigationBar from 'react-native-navbar'
import Button from 'react-native-button';
import React, { Component, } from 'react'
import TextField from 'react-native-md-textinput';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {
  View,
  Image,
  StyleSheet,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  StatusBar
} from 'react-native'

class Splash extends Component {

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)
    this.state = {}
  }
  
  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={false} showHideTransition={'slide'} animated={true} />
        <NavigationBar
          title={{ title: '', tintColor: '#000', }}
          style={{ backgroundColor: "#fff", }}
          statusBar={{ tintColor: "#fff", }}
        /> 
        
        <KeyboardAwareScrollView 
          style={styles.scrollContainer} 
          ref="scroll" 
          onKeyboardWillHide={() => this.refs.scroll.scrollToPosition(0, 0, true)}>
          
          <Image style={styles.logo} resizeMode={"contain"} source={require('../../images/splash-logo.png')} />
          
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'space-between'}}>

            <View style={styles.field}>
              <Button
                containerStyle={styles.goButtonContainer}
                style={styles.goButtonText}
                onPress={() => this.props.navigator.push({ name: "Map" })}>
                Let the hunt begin!
              </Button>
            </View>
          </View>
          
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    backgroundColor:'#fff',
    flex: 1
  },
  scrollContainer: {
    flex: 1,
  },
  logo:{
    height: 400,
    alignSelf: "center",
  },
  field: {
    width: 250,
    marginTop: 10
  },
  goButtonContainer: {
    padding:10, 
    height:45, 
    overflow:'hidden', 
    borderRadius:4, 
    borderColor: BaseStyles.colors.primary,
    backgroundColor: BaseStyles.colors.primary, 
    borderWidth: 1, 
    flex: 1,
    marginTop: 20
  },
  goButtonText: {
    fontSize: 20, 
    color: 'white'
  }
});

export default Splash