import React, { Component, } from 'react'
import NavigationBar from 'react-native-navbar'
import BaseStyles from '../../styles/base'
import {
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  StatusBar,
  WebView,
} from 'react-native'

class WebDetails extends Component {

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const backButton = 
      <TouchableOpacity onPress={() => this.props.navigator.pop()} activeOpacity={0.3}>
        <Image style={styles.backButton} resizeMode={"contain"} source={require('../../images/left-arrow.png')} />
      </TouchableOpacity>
          
    return (
      <View style={styles.container}>
        <StatusBar hidden={false} showHideTransition={'slide'} animated={true} barStyle="light-content" />
        <NavigationBar
          title={{ title: this.props.data.title, tintColor: 'white', }}
          leftButton={backButton}
          style={{ backgroundColor: BaseStyles.colors.primary, }}
          statusBar={{ tintColor: BaseStyles.colors.primary, }}
        />
      
        <WebView
          source={{uri: this.props.data.url}}
          startInLoadingState={true}
          />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    backgroundColor: "#fff",
    flex: 1,
  },
  backButton:{
    width: 19,
    height: 25,
    marginTop: 10,
    marginLeft: 7
  },
});

export default WebDetails