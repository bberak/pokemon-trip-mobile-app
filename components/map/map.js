import NavigationBar from 'react-native-navbar'
import React, { Component, } from 'react'
import BaseStyles from '../../styles/base'
import PokemonMarker from './pokemonMarker'
import TripList from './tripList'
import {
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  Text,
  Navigator,
  StatusBar
} from 'react-native'
import MapView from 'react-native-maps';
import Modal from 'react-native-modalbox'

class Map extends Component {

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)
    this.state = {
      pokemon: [],
      stops: [],
      selectedStop: {},
      selectedTrip: {}
    }
    
    this.scanForPokemon();
    this.fetchStops();
  }
  
  scanForPokemon = () => {
    const distance = 4000;
    navigator.geolocation.getCurrentPosition((pos) => {
      return fetch('http://pokemon-trip-api-dev.ap-southeast-2.elasticbeanstalk.com/scan/' + pos.coords.latitude + '/' + pos.coords.longitude + '/' + distance)
        .then((res) => res.json())
        .then((list) => {
          this.setState({
            pokemon: list.map(x => ({latitude: x.latitude, longitude: x.longitude, name: x.name, despawns_in_str: x.despawns_in_str, despawn_time: x.despawn_time, id: x.id, image: x.image}))
          });
          setTimeout(this.scanForPokemon, 30000);
        })
        .catch((error) => {
          setTimeout(this.scanForPokemon, 2000);
        });
      },
      (error) => {
        setTimeout(this.scanForPokemon, 2000);
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 100});
  }
  
  fetchStops = () => {
    const radius = 0.3;
    navigator.geolocation.getCurrentPosition((pos) => {
      //return fetch('http://192.168.2.183:3000/stops?lon=' + pos.coords.longitude + '&lat=' + pos.coords.latitude + '&search_radius=' + radius)
      return fetch('http://pokemon-trip-api-dev.ap-southeast-2.elasticbeanstalk.com/stops/-33.8858330/151.2075480/0.3')
        .then((res) => res.json())
        .then((list) => {
          this.setState({
            stops: list.map(x => ({latitude: x.stop_lat, longitude: x.stop_lon, id: x._id, name: x.stop_name}))
          });
          setTimeout(this.fetchStops, 900000);
        })
        .catch((error) => {
          setTimeout(this.fetchStops, 2000);
        });
      },
      (error) => {
        setTimeout(this.fetchStops, 2000);
      },
      {enableHighAccuracy: false, timeout: 20000, maximumAge: 100});
  }
  
  onStopSelected = (id, name) => {
    this.refs.modal.open();
    this.setState({
      selectedStop: {
        id: id,
        name: name
      }
    });
  }
  
  onTripClick = (tripData) => {
    this.setState({
      selectedTrip: tripData
    });
  };
    
  render() {   
    const modal = 
        <Modal style={[styles.modal]} position={"bottom"} ref={"modal"} swipeArea={30} swipeToClose={true} backdrop={false}>
          <TripList name={this.state.selectedStop.name} id={this.state.selectedStop.id} onTripClick={this.onTripClick} />
        </Modal>;
    
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} showHideTransition={'slide'} animated={true} />
        <MapView
          style={styles.map}
          showsPointsOfInterest={false}
          showsUserLocation={true}
          followsUserLocation={true}
          showsCompass={true}
          pitchEnabled={true}
          loadingEnabled={true}
          initialRegion={{
            latitude: 0,
            longitude: 0,
            latitudeDelta: 0.0922 * 0.01,
            longitudeDelta: 0.0421 * 0.01,
          }}>
          {this.state.pokemon.map(x => (
            <MapView.Marker
              key={x.id}
              coordinate={{latitude: x.latitude, longitude: x.longitude}}
              title={x.name}
              description={x.despawns_in_str + " mins"}>
              <PokemonMarker image={x.image} />
            </MapView.Marker>
          ))}
          {this.state.stops.map(x => (
            <MapView.Marker
              key={x.id}
              coordinate={{latitude: x.latitude, longitude: x.longitude}}
              pinColor={x.id == this.state.selectedStop.id ? BaseStyles.colors.highlight : BaseStyles.colors.secondary }
              onSelect={() => this.onStopSelected(x.id, x.name)}/>
          ))}
          <MapView.Polyline
            coordinates={this.state.selectedTrip.coordinates}
            strokeWidth={5}
            strokeColor={BaseStyles.pastelColors.purple}
            lineCap={"round"}/>
        </MapView>
        {modal}
      </View>
    )        
  }
}

const styles = StyleSheet.create({
  container:{
    backgroundColor:'#fff',
    flex: 1
  },
  map: {
    flex: 1
  },
  modal: {
    height: 200,
  }
});
      
export default Map