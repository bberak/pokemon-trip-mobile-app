import React, { Component, } from 'react'
import { View, ScrollView, Text, StyleSheet, RefreshControl, TouchableOpacity } from 'react-native'
import BaseStyles from '../../styles/base'

class TripList extends Component {

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)
    this.state = {
      isRefreshing: true,
      rowData: []
    }
    
    this.onRefresh();
  }
  
  onRefresh = () => {
    if (this.state.isRefreshing == false)
      this.setState({isRefreshing: true});
    
    return fetch('http://pokemon-trip-api-dev.ap-southeast-2.elasticbeanstalk.com/trips/' + this.props.id)
      .then((res) => res.json())
      .then((list) => {
        this.setState({
          rowData: list.map(x => ({id: x.id, name: x.name})),
          isRefreshing: false
        });
      })
      .catch((error) => {
        setTimeout(this.onRefresh, 2000);
      });
  };
  
  onTripClick = (tripId) => {
    this.props.onTripClick({
      tripId: tripId, 
      name: "10:09 AM, Platform 28",
      coordinates: [
        {latitude: -33.8858330 + 0.001, longitude: 151.2075480 + 0.001},
		{latitude: -33.8858330 + 0.001, longitude: 151.2075480 + 0.001},
		{latitude: -34.8858330 + 0.001, longitude: 151.2075480 + 0.001},
		{latitude: -33.8858330 + 0.001, longitude: 151.2075480 + 0.001},
		{latitude: -33.8858330 + 0.001, longitude: 152.2075480 + 0.001}]
    })
  };

  render() {
    const refreshControl = 
      <RefreshControl
        refreshing={this.state.isRefreshing}
        onRefresh={this.onRefresh}/>
          
    const rows = this.state.rowData.map((row, ii) => {
      return <TouchableOpacity key={row.id} onPress={() => this.onTripClick(row.id)}><View><Text style={styles.itemText}>{row.name}</Text></View></TouchableOpacity>;
    });
          
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.stopText}>{this.props.name}</Text>
        </View>
        <ScrollView refreshControl={refreshControl} style={styles.scrollview} containerContentStyle={styles.scrollViewContent}>
          <View style={styles.scrollViewContent}>
            {rows}
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    backgroundColor:'#FFF',
    flex: 1,
    shadowColor: "#333",
    shadowOpacity: 0.6,
    shadowRadius: 6
  },
  scrollview: {
    flex: 1,
  },
  scrollViewContent: {
    justifyContent: 'center',
    alignItems: 'center',
    width: window.width,
    flex: 1,
  },
  header: {
    paddingTop: 2,
    borderBottomWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5
  },
  stopText: {
    height: 30,
    fontSize: 20,
    fontWeight: "bold",
  },
  itemText: {
    height: 30,
    fontSize: 20,
  }
});

export default TripList