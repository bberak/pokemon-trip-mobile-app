import React, { Component, } from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'

class PokemonMarker extends Component {

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <View>
        <Image style={styles.image} source={{uri: this.props.image}} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
  },
  bubble: {
    flex: 0,
    flexDirection: 'row',
    alignSelf: 'flex-start'
  },
  image: {
    width: 45,
    height: 45
  }
});

export default PokemonMarker